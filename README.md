# Quick and dirty mysql backup container

## Ambient config

This container looks for the following config from the environment:

NAME | NOTES |
---- | ----- |
DATABASE_URL | `mysql://USERNAME:PASSWORD@HOST:PORT/DATABASE` |
DESTINATION | `username@hostname:path`. Used in the command `scp $FILENAME $DESTINATION/FILENAME` |

## Extra config you need to provide

- You'll need to do something to make ssh work. This most likely
  means dropping suitable config files under `/root/.ssh`

# Deployment

A sample `deploy.yaml` is provided to demonstrate how this could be
deployed. This config refers to several secrets; sample values for
those secrets are not provided but the format should be easy to determine.

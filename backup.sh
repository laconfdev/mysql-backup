#!/bin/sh

set -e
set -x

USERNAME=$(echo ${DATABASE_URL} | sed -n 's|^mysql://\(.*\):\(.*\)@\(.*\):\(.*\)/\(.*\)$|\1|p')
PASSWORD=$(echo ${DATABASE_URL} | sed -n 's|^mysql://\(.*\):\(.*\)@\(.*\):\(.*\)/\(.*\)$|\2|p')
HOST=$(echo ${DATABASE_URL} | sed -n 's|^mysql://\(.*\):\(.*\)@\(.*\):\(.*\)/\(.*\)$|\3|p')
PORT=$(echo ${DATABASE_URL} | sed -n 's|^mysql://\(.*\):\(.*\)@\(.*\):\(.*\)/\(.*\)$|\4|p')
DATABASE=$(echo ${DATABASE_URL} | sed -n 's|^mysql://\(.*\):\(.*\)@\(.*\):\(.*\)/\(.*\)$|\5|p')
FILENAME="${DATABASE}-$(date +%Y-%m-%d).sql.gz"

date

/usr/bin/mysqldump -h $HOST -P $PORT -u $USERNAME --password=${PASSWORD} --no-tablespaces ${DATABASE} | gzip > $FILENAME
scp $FILENAME $DESTINATION/$FILENAME

rm $FILENAME

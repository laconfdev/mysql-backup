FROM pygmy/alpine-tini:latest

RUN apk add --no-cache dcron openssh-client mariadb-client && \
    rm -rf /etc/cron.daily /etc/cron.weekly
COPY crontab /var/spool/cron/crontabs/root
COPY backup.sh /
CMD ["/usr/sbin/crond", "-f"]
